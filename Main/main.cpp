#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
int count10, count20, count50;
const double value10 = 0.10, value20 = 0.20, value50 = 0.50;

cout << "Enter 10s count: ";
cin >> count10;

cout << "Enter 20s count: ";
cin >> count20;

cout << "Enter 50s count: ";
cin >> count50;

double result = count10 * value10 + count20 * value20 + count50 * value50;
cout << "You have" << fixed << setprecision(2) << result << " BGN" << endl;



return 0;
}
