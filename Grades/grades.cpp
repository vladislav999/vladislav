#include <iostream>
#include <cmath>
#include <string>
#include <iomanip>

using namespace std;

bool isValidGrade(double grade)

{
    return grade >= 2 && grade <= 6;
}

int main()
{
    string fname, lname , fnumber;
    cin >> fname >> lname >> fnumber;

    double grade1 , grade2, grade3;
    cin >> grade1 >> grade2 >> grade3;

    bool areGradesValid=isValidGrade(grade1) && isValidGrade(grade2) && isValidGrade(grade3);
    if(!areGradesValid)
    {
        cout << "Invalid grades" << endl;
        return -1;
    }

    double avg = (grade1 +grade2 +grade3)/3;
    cout << setw(10) << "Faculty N" << setw(10) << "Avg" << endl;
    cout << setw(10) << fnumber << setw(10) << avg << endl;
    cout << avg;

    return 0;
}
